from flask import Flask
from app import blueprints

# Factory app
def create_app():

	app = Flask(__name__)

	# Extensions

	# Blueprints
	blueprints.configure(app)

	return app