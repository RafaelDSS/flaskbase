from app.blueprints import api, views

def configure(app):
	api.configure(app)
	views.configure(app)