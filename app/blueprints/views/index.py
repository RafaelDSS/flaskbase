from flask import Blueprint, render_template

app_index = Blueprint('index', __name__)

@app_index.route('/')
def index():
	return render_template('index.html')

def configure(app):
	app.register_blueprint(app_index)