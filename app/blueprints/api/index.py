from flask import Blueprint, jsonify

app_api = Blueprint('api', __name__)

@app_api.route("/api/index")
def api_index():
	return jsonify({'status':True})

def configure(app):
	app.register_blueprint(app_api)